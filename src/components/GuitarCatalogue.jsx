import React, {useEffect} from "react";
import withUser from "../hoc/withUser";
import {useGuitarContext} from "../context/GuitarContext";

const GuitarCatalogue = () => {

    console.log('GuitarCatalogue.render')
    const { guitarState, dispatch } = useGuitarContext()

    useEffect(() => {
        const loadGuitars = async () => {
            try {
                const response = await fetch('http://localhost:8000/guitars')
                const guitars = await response.json()
                dispatch({ type: 'SET_GUITARS', payload: guitars })
            }
            catch (e) {
                dispatch({ type: 'SET_GUITARS_ERROR', payload: e.message })
            }
        }

        loadGuitars()
    }, [])

    return (
        <>
            <h2>Guitars</h2>
            { guitarState.loading && <p>Loading guitars...</p> }
            <ul>
                { guitarState.guitars.map(guitar => <li key={ guitar.id }>{ guitar.model }</li>) }
            </ul>
        </>
    )
}

export default withUser(GuitarCatalogue)