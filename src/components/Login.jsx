import React, {useEffect} from "react";
import {useUser} from "../context/UserContext";
import {useHistory} from "react-router-dom";

const Login = () => {

    console.log('Login.render')

    const { user, setUser } = useUser()
    const history = useHistory()

    const handleLoginClick = () => {
        console.log('Login.handleLoginClick')
        setUser('dewaldels')
    }

    useEffect(() => {
        console.log('Login.useEffect')
        if (user !== '') {
            history.push('/guitars')
        }
    }, [ user ])

    return (
        <>
            <h2>Login</h2>
            <button onClick={ handleLoginClick }>Login</button>
        </>
    )
}

export default Login