import React from "react";
import styles from './Navbar.module.css'
import {useUser} from "../context/UserContext";
import {useGuitarContext} from "../context/GuitarContext";

const Navbar = () => {
    console.log('Navbar.render')

    const {  user } = useUser()
    const { guitarState } = useGuitarContext()


    return (
        <nav className={ styles.Navbar }>
            <ul className={styles.NavbarMenu}>
                <li className={ styles.NavbarMenuItem}><b>Guitar Catalogue</b></li>
                { user &&
                <>
                    <li className={ styles.NavbarMenuItem}>{ user }</li>
                    <li className={ styles.NavbarMenuItem}>{ guitarState.guitars.length }</li>
                </>
                }

            </ul>
        </nav>
    )
}

export default Navbar