import React from 'react'
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Login from "./components/Login";
import GuitarCatalogue from "./components/GuitarCatalogue";
import Navbar from "./components/Navbar";

function App() {

    console.log('App.render')

    return (
        <BrowserRouter>
            <Navbar />
            <Switch>
                <Route path="/" exact component={ Login } />
                <Route path="/guitars" component={ GuitarCatalogue } />
            </Switch>
        </BrowserRouter>

    )
}

export default App
