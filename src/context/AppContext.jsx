import React from "react";
import UserProvider from "./UserContext";
import GuitarProvider from "./GuitarContext";

const AppContext = ({ children }) => {
    return (
        <UserProvider>
            <GuitarProvider>
                { children }
            </GuitarProvider>
        </UserProvider>
    )
}
export default AppContext