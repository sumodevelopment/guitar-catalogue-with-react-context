import React, {createContext, useContext, useReducer} from "react";

const GuitarContext = createContext(null)

export const useGuitarContext = () => {
    return useContext(GuitarContext)
}

const guitarReducer = (state, action) => {
    switch (action.type) {
        case 'SET_GUITARS':
            return {
                loading: false,
                error: '',
                guitars: action.payload
            }
        case 'SET_GUITARS_ERROR':
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        default:
            return state
    }
}

const initialState = {
    loading: true,
    error: '',
    guitars: []
}

const GuitarProvider = ({children}) => {

    const [ guitarState, dispatch ] = useReducer(guitarReducer, initialState)

    return (
        <GuitarContext.Provider value={{ guitarState, dispatch }}>
            {children}
        </GuitarContext.Provider>
    )
}

export default GuitarProvider